using System;
using Stylet;
using StyletIoC;
using GateWpfMVVM.Pages;

namespace GateWpfMVVM
{
    public class Bootstrapper : Bootstrapper<ContainerViewModel>
    {
        protected override void ConfigureIoC(IStyletIoCBuilder builder)
        {
            // Configure the IoC container in here
        }

        protected override void Configure()
        {
            // Perform any other configuration before the application starts
        }
    }
}
