using Stylet;

namespace GateWpfMVVM.Pages
{
    public class ContainerViewModel : Screen 
    {
        
        public AxVeconclientProj.AxVECONclient axVECONclient = new AxVeconclientProj.AxVECONclient();

        public ContainerViewModel()
        {           
            axVECONclient.CreateControl();
            axVECONclient.ServerIPAddr = "127.0.0.1";
            axVECONclient.ServerPort = 12011;

            axVECONclient.OnNewLPNEvent += AxVECONclient_OnNewLPNEvent;
            axVECONclient.OnUpdateLPNEvent += AxVECONclient_OnUpdateLPNEvent;
            axVECONclient.OnCombinedRecognitionResultISO += AxVECONclient_OnCombinedRecognitionResultISO;
        }

        /// <summary>
        /// 箱号结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AxVECONclient_OnCombinedRecognitionResultISO(object sender, AxVeconclientProj.IVECONclientEvents_OnCombinedRecognitionResultISOEvent e)
        {
            Time = e.triggerTime.ToString("yyyy-MM-dd HH:mm:ss");
            Con1 = e.containerNum1;
            IOS1 = e.iSO1;
            Check1 = e.checkSum1;
            Con2 = e.containerNum2;
            IOS2 = e.iSO2;
            Check2 = e.checkSum2;
            switch (e.containerType)
            {
                case -1:Type = "未知"; break;
                case 0: Type = "20 吋集装箱"; break;
                case 1: Type = "40 吋集装箱"; break;
                case 2: Type = "两个 20 吋集装箱"; break;
            }
        }

        /// <summary>
        /// 重车车牌
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AxVECONclient_OnUpdateLPNEvent(object sender, AxVeconclientProj.IVECONclientEvents_OnUpdateLPNEventEvent e)
        {
            Plate = e.lPN;
            Lane = e.laneNum.ToString();
            Time = e.triggerTime.ToString("yyyy-MM-dd HH:mm:ss");
            Color = e.colorCode.ToString();
        }

        /// <summary>
        /// 空车车牌结果
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AxVECONclient_OnNewLPNEvent(object sender, AxVeconclientProj.IVECONclientEvents_OnNewLPNEventEvent e)
        {
            Plate = e.lPN;
            Lane = e.laneNum.ToString();
            Time = e.triggerTime.ToString("yyyy-MM-dd HH:mm:ss");
            Color = e.colorCode.ToString();
        }

        /// <summary>
        /// 链接箱号
        /// </summary>
        public void Connect2Server()
        {
            axVECONclient.Connect2Server();
        }

        /// <summary>
        /// 车牌
        /// </summary>
        private string _plate;
        public string Plate
        {
            get => this._plate;
            set => SetAndNotify(ref _plate, value);
        }

        /// <summary>
        /// 车道号
        /// </summary>
        private string _lane;
        public string Lane
        {
            get => this._lane;
            set => SetAndNotify(ref _lane, value);
        }

        /// <summary>
        /// 过闸时间
        /// </summary>
        private string _time;
        public string Time
        {
            get => this._time;
            set => SetAndNotify(ref _time, value);
        }

        /// <summary>
        /// 车牌颜色
        /// </summary>
        private string _color;
        public string Color
        {
            get => this._color;
            set => SetAndNotify(ref _color, value);
        }

        /// <summary>
        /// 前箱号
        /// </summary>
        private string _con1;
        public string Con1
        {
            get => this._con1;
            set => SetAndNotify(ref _con1, value);
        }

        /// <summary>
        /// 后箱号
        /// </summary>
        private string _con2;
        public string Con2
        {
            get => this._con2;
            set => SetAndNotify(ref _con2, value);
        }

        /// <summary>
        /// 前箱号IOS
        /// </summary>
        private string _ios1;
        public string IOS1
        {
            get => this._ios1;
            set => SetAndNotify(ref _ios1, value);
        }

        /// <summary>
        /// 后箱号IOS2
        /// </summary>
        private string _ios2;
        public string IOS2
        {
            get => this._ios2;
            set => SetAndNotify(ref _ios2, value);
        }

        /// <summary>
        /// 前箱号识别状态
        /// </summary>
        private string _check1;
        public string Check1
        {
            get => this._check1;
            set => SetAndNotify(ref _check1, value);
        }

        /// <summary>
        /// 后箱号识别状态
        /// </summary>
        private string _check2;
        public string Check2
        {
            get => this._check2;
            set => SetAndNotify(ref _check2, value);
        }

        /// <summary>
        /// 箱型结果
        /// </summary>
        private string _type;
        public string Type
        {
            get=> this._type;
            set => SetAndNotify(ref _type, value);
        }
    }
}
